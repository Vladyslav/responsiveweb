class ExercisesController < ApplicationController
  # GET /exercises
  # GET /exercises.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @exercises }
    end
  end
end
