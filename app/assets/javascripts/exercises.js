$(document).ready(function(){

    $(".mediaList img").click(function(el){
        scaleExerciseImage(el);
    });

    $('.exercise .intro').click(function(el){
        showExerciseDetails(el);
    });

    $('.rotateMap').click(function(el){
        rotateBodyMap(el);
    });

    $('.muscleButton').click(function(el){
        activateMuscleButton(el);
    });

    $('.muscleFilter .editFilter').click(function(el){
        showMuscleMap(el);
    });

    $('.muscleMap .doneEditing').click(function(el){
        hideMuscleMap(el);
    });

    function hideMuscleMap(el){
        $('.muscleMap').slideUp(300);
        $('.muscleFilter .editFilter').show();
    }

    function showMuscleMap(el){
        $('.muscleMap').slideDown(300);
        $(el.target).hide();
    }

    function activateMuscleButton(el){
        var target = $(el.target);
        var id = target.attr('id');

        if (toggleActive(target)){
            addMuscleFilter(id, target.text());
        } else {
            removeMuscleFilter(id);
        }
    }
//TODO collapse to all muscles tag when
    function addMuscleFilter(id, text){
        if ($('#allMuscles').length > 0){
            $('#allMuscles').hide();
        }
        $('.muscleList .editFilter').before("<div class='selectedMuscle' id='" + id + "' > " + text + "</div>");
    }

    function removeMuscleFilter(id){
        $('.muscleList #'+id).remove();

        if ($('.selectedMuscle').length <= 1) {
            $('#allMuscles').show();
        }
    }

    function rotateBodyMap(el){
        var muscleMap = $('.muscleMap');

        if (muscleMap.hasClass('backView')) {
            muscleMap.removeClass('backView');
        } else {
            muscleMap.addClass('backView');
        }
    }

//    todo ajax loading data
    function showExerciseDetails(el){
        toggleActive($(el.target).parents('.exercise'))
    }

    function toggleActive(element){
        if (element.hasClass('active')) {
            element.removeClass('active');
            return false;
        } else {
            element.addClass('active');
            return true;
        }
    }

    function scaleExerciseImage(el){
        var target = $(el.target);
        var mediaCont = target.parent();

        if (mediaCont.hasClass('active')){
            mediaCont.removeClass('active');
        } else {
            $('.mediaList .active').removeClass('active');
            mediaCont.addClass('active');
        }

    }

});