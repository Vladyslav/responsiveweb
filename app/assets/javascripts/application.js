// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .
//= require twitter/bootstrap

//stickers adding
//TODO fix UI styles
(function($) {
		$.informMessage = function(o) {
			var o = $.extend({
				time:5000,
				speed:'slow',
				note:null,
				title:null,
				className:'inform',
				sticked:false,
				position:{top:0,right:'50px'}
			}, o);
			var stickers = $('#informMessageBlock');
			if (!stickers.length) {
				$('body').prepend('<div id="informMessageBlock"></div>');
				var stickers = $('#informMessageBlock');
			}
			stickers.css({top:'auto',bottom:'auto'}).css(o.position);
			var stick = $('<div class="informMessage">'+
					'<div class="informMessageHolder"><div class=informMessageCont><div class="info_pic"></div>'+
					'<div class="infoTitle"></div><div class="informMessageText"></div>'+
					'<div class="exitInformMessage"></div><div class="clear"></div></div></div></div>');
			stickers.append(stick);
			if (o.className) {
				stick.addClass(o.className);// class type: error/inform/success
				switch (o.className){
					case 'error':
						o.title = "Error:";
						break;
					case 'inform':
						o.title = "Information:";
						break;
					case 'success':
						o.title = "Success:";
						break;
					default :
						o.title = "Message:";
				}
			}
	        stick.find('.informMessageText').html(o.note);
	        stick.find('.infoTitle').html(o.title);
	        var height = stick.find('.informMessageHolder').css('height');
	        stick.css('height',height);
	        stick.find('.informMessageBackground').css('height',height);

			stick.find('.exitInformMessage').click(function(){
				stick.fadeOut(o.speed,function(){
					$(this).remove();
				})
			});
			if (!o.sticked) {
				setTimeout(function(){
					stick.fadeOut(o.speed,function(){
						$(this).remove();
					});
				}, o.time);
			}
		};

	})(jQuery);




function goAjax(urlAddress,dataObject,successFunction,errorFunction,el){
	showAnimation(el);
	$.ajax({
		  type: "POST",
		  url: urlAddress,
		  dataType: "json",
		  contentType: "application/json; charset=utf-8",
		  processData: false,
		  data: JSON.stringify(dataObject),
		  context: document.body,
		  success: function(data,status){hideAnimation(el); successFunction(data,status);},
		  error: function(data, status, xml){
			  hideAnimation(el); errorFunction();
			  detectStatusRedirect(data);
		  }
	});
}

function goAjaxWithoutAnimation(urlAddress,dataObject,successFunction,errorFunction){
	$.ajax({
		  type: "POST",
		  url: urlAddress,
		  dataType: "json",
		  contentType: "application/json; charset=utf-8",
		  processData: false,
		  data: JSON.stringify(dataObject),
		  context: document.body,
		  success: function(data,status){successFunction(data,status);},
		  error: function(data,status){errorFunction(); detectStatusRedirect(data);}
	});
}

function ajaxLoad(urlAddress,dataObject,successFunction,errorFunction,el){
			showAnimation(el);
			$.post(urlAddress.split(" ")[0],dataObject,
					function(response,status,xhr){
						hideAnimation(el);
						if (!detectStatusRedirect(xhr)){
							if ((status == 'error')||($(response).find(urlAddress.split(" ")[1]).length==0)){
								console.log(xhr.statusText);
								errorFunction;
								sorry();
								$.address.value(lastAddress);
							} else {
								window.scroll(0,0);
								$('#ajaxContent').html($(response).find(urlAddress.split(" ")[1]));
								$('.backToDashboard').click(function(){
									$.address.value("");
								});
								$('.recentEvents').hide();
								successFunction();
								lastAddress = $.address.value().substring(1);
							}
						}
					}
				).error(function(data){sorry("Sorry, you should log in."); hideAnimation(el); detectStatusRedirect(data); });
		}

function ajaxFullLoad(target,urlAddress,dataObject,successFunction,errorFunction,el){
			showAnimation(el);
			target.load(urlAddress,dataObject,
			function(response,status,xhr){
				hideAnimation(el);
				if (!detectStatusRedirect(xhr)){
					if (status == 'error'){
						console.log(xhr.statusText);
						errorFunction();
						sorry();
					} else {
						window.scroll(0,0);
						$('.tableName').click(function(){
							$(this).parents('#dasboardBlock').find('.recentEvents').show();
							$(this).parents('#ajaxContent').children().remove();
							resetActiveTabs();
						});
						$('.recentEvents').hide();
						successFunction();
					}
				}
			},function(data){sorry(); hideAnimation(el); detectStatusRedirect(data);}
		);}

//	TODO add #loading block
	//  ajax animattion block
		function showAnimation(el){
			$('.inform_block').remove();
			var imgLoad = $('#loading');
			imgLoad.show();
			var centerY;
			var centerX;

			if (el==null || el == undefined){
				centerY = $(window).scrollTop() + ($(window).height() + imgLoad.height())/2;
				centerX = $(window).scrollLeft() + ($(window).width() + imgLoad.width())/2;
			} else {
				centerY = el.pageY-$(el.currentTarget).height()/2;
				centerX = el.pageX;
				if (centerY == null || isNaN(centerY)){
					centerY = $(window).scrollTop() + ($(window).height() + imgLoad.height())/2;
				}
				if (centerX == null){
					centerX = $(window).scrollLeft() + ($(window).width() + imgLoad.width())/2;
				}
			}

			imgLoad.offset({top:centerY,left:centerX});
		}

		function showLocaleAnimation(target){
			showLocaleAnimation("", target)
		}

		function showLocaleAnimation(preUrl, target){
//			$('.inform_block').remove();
			target.before("<div class=loading-img ><img id=loader src='" + preUrl + "img/ajax-loader.gif' /></div>");
			target.hide();
		}

		function hideLocaleAnimation(target){
			target.siblings('.loading-img').remove();
			target.show();
		}
		function hideAnimation(){
			$('#loading').hide();
		}


